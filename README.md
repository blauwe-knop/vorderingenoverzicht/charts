# charts

Todo change readme.

```sh
keybase pgp gen
...
keybase pgp export -s | gpg --import
gpg --list-secret-keys
gpg --export-secret-keys >~/.gnupg/secring.gpg
```

```sh
cd public/repo

helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../scheme-db/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../scheme-management-ui/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../scheme-process/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../scheme-service/helm

helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../bk-config-db/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../bk-config-service/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../bk-management-process/helm
helm package --sign --key "VO Rijk" --keyring ~/.gnupg/secring.gpg ../../../bk-management-ui/helm
```

```sh
cd public && helm repo index --url https://blauwe-knop.gitlab.io/vorderingenoverzicht/charts .
```
